<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" xmlns="http://www.w3.org/1999/xhtml">

<head>
 <title><?php print $head_title ?></title>
 <?php print $head ?>
 <?php print $scripts ?>
 <?php print $styles ?>
 <!--[if lte IE 6]>
 <style type="text/css" media="screen">
   /* IE min-width trick */
   div#wrapper1 { 
     width:expression(((document.compatMode && document.compatMode=='CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth) < 720 ? "720px" : "auto"); 
     }
  </style>
  <![endif]-->
</head>

<body <?php print theme("onload_attribute"); ?>>
<div class="hide">
 <?php if ($site_slogan) : ?>
  <div id="site-slogan"><span><?php print($site_slogan) ?></span></div>
 <?php endif;?>
 <a href="#content" title="Skip the site navigation to go directly to the content">Skip to content</a>
</div>
<!-- closes #header-->
<!-- START: HEADER -->
<div id="wrapper1">
 <div id="wrapper2">
  <div class="header" id="header">
   <div class="headerpadding">
    <?php if ($logo) : ?>
     <div id="site-name"><a href="<?php print $base_path ?>" title="Home"><img
      src="<?php print $logo ?>" alt="<?php print $site_name ?> Logo" /></a></div>
    <?php endif; ?>
    
    <div id ="region_header"><?php print $header ?></div>
    
    <?php if (module_exists('banner')) : ?>
     <div id="banner"><?php print banner_display() ?></div>
    <?php endif; ?>

    <?php if ($primary_links) : ?>
     <?php print theme('links', array_reverse($primary_links), array('class' =>'links', 'id' => 'primary')); ?>
    <?php elseif ($secondary_links) : ?>
     <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'secondary')); ?>
    <?php endif; ?>

    <?php if ($search_box) : ?>
     <?php print $search_box ?> 
    <?php endif; ?>   
      
    </div>
   </div>
<!-- END: HEADER-->
<hr class="hide" />  
    <div class="columns">
      <?php if ($left != '') { ?>
      <div class="leftcolumn sidebar" id="sidebar-left">
        <div class="leftpadding">
            <?php print $left; ?>
          </div>
        </div>
      <?php } ?>
      <?php if ($right != '') { ?>
      <div class="rightcolumn sidebar" id="sidebar-right">
        <div class="rightpadding">
          <?php print $right; ?>
        </div>
      </div>
      <?php } ?>
      <hr class="hide" />  
      <div class="centercolumn">
        <div class="centerpadding">
          <div class="main-content" id="main">
            <?php if (($_GET['q']) != variable_get('site_frontpage','node')): ?>

              <?php if ($breadcrumb != ""): ?>
                <div id="breadcrumbs">
                  <?php print t('You are here: ').$breadcrumb;?> <span class="breadcrumb"> &raquo;  <?php if ($title != ""): print ucwords($title); endif; ?></span>
                </div>
              <?php endif; ?>
            <?php endif; ?>
            <?php if ($mission != ""): ?>
              <div id="mission"><span><?php print $mission ?></span></div>
            <?php endif; ?>
            <?php if ($title != ""): ?>
              <h1 id="title"><?php print $title ?></h1>
            <?php endif; ?>
            <?php if ($messages != ""): ?>
              <div id="message"><?php print $messages ?></div>
            <?php endif; ?>
            <?php if ($help != ""): ?>
              <div id="help"><?php print $help ?></div>
            <?php endif; ?>
	    <?php if ($tabs != ""): ?>
		<?php print $tabs ?>
            <?php endif; ?>

            <!-- start main content -->
            <span><a name="content"></a></span>
	    <div class="content">
              <?php print $content ?>
              <?php if ($signature && $comment->cid > 1234): // Change "1234" to the last comment ID used before upgrading to Drupal 6 ?>
                <div class="user-signature clear-block">
                  <?php print $signature ?>
                </div>
              <?php endif; ?>
           </div>
            <!-- end main content -->

          </div><!-- main -->
        </div>
      </div>
    </div>
    <div class="clearing"></div>
    <hr class="hide" />
<!-- START: FOOTER-->
    <div id="footer" class="footer">
      <?php if ($footer) : ?>
        <?php print $footer;?>
      <?php endif; ?>
      <?php if ($footer_message) : ?>
        <?php print $footer_message;?>
      <?php endif; ?>
      <?php print $closure;?>
    </div>
<!-- END: FOOTER -->
	</div><!-- wrapper -->
</div><!-- outer_wrapper -->
</body>
</html>

