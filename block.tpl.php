<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?>">
<?php if ($block->subject): ?>
  <h2><span><?php print $block->subject ?></span></h2>
<?php endif;?>

<div class="content"><?php print $block->content ?></div>
</div>

